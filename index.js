    var express = require('express');
    var app = express();
    var server = require('http').createServer(app);
    var io = require('socket.io').listen(server);
    var fs = require('fs');
    var currentStore = fs.readFileSync('data.json', 'utf-8');
    var resArray = JSON.parse(currentStore);
    const port = process.env.PORT || 10000;

server.listen(port);


app.get('/', function(request, response){
    response.sendFile(__dirname + '/index.html');
});

users = [];
connections = [];

io.sockets.on('connection', function(socket) {
    console.log("Успешное соединение");
    connections.push(socket);

  socket.on('disconnect', function(data) {
    connections.splice(connections.indexOf(socket), 1);
    console.log("Отключились");
  });

  socket.on('send mess', function(data) {
    io.sockets.emit('add mess', {mess: data.mess, name: data.name});
    resArray.push(data);
    fs.writeFileSync('data.json', JSON.stringify(resArray));
  });
});